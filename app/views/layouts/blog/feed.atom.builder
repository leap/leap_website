atom_feed(:language => I18n.locale.to_s) do |feed|
  feed.title "LEAP News"
  feed.updated @pages.first.props.posted_at
  @pages.each do |page|
    feed.entry(page, :url => page_path(page), :updated => page.props.posted_at) do |entry|
      entry.title page.title
      entry.summary page.props.preview, :type => 'html'
      entry.content page_body(page), :type => 'html'
      entry.author page.props.author
    end
  end
end
