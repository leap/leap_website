module ApplicationHelper

  def main_column_class
    if has_side_column?
      'with_side_column'
    else
      'full'
    end
  end

  #
  # three forms:
  #
  #  (1) link('page-name')
  #  (2) link('label' => 'page-name')
  #  (3) link('label' => 'https://url')
  #
  # both accept optional options hash:
  #
  #  (1) link('page-name', :class => 'x')
  #  (2) link('label' => 'page-name', :class => 'x')
  #
  def link(name, options=nil)
    if name.is_a? Hash
      klass = name.delete(:class)
      label, name = name.to_a.first
      if label.is_a? Symbol
        label = I18n.t label
      end
    else
      klass = options[:class] if options
    end
    if name.starts_with?('#') || name.starts_with?('http')
      path = name
    else
      page = site.find_page(name)
      if page
        label ||= page.title
        path = page_path(page)
      else
        label = '[dead link]'
        path = '/'
      end
    end
    link_to label, path, {:class => klass}
  end

  def page_path(page)
    if page.props.path_prefix
      "/#{I18n.locale.to_s}/#{page.props.path_prefix}/#{page.name}"
    else
      '/' + ([I18n.locale.to_s] + page.path).join('/')
    end
  end

  #
  # allows a template to render a partial giving a relative path (relative to the calling template)
  #
  def render_local_template(arg, locals={})
    caller_path = Pathname.new(caller.first.gsub(/^([^:]*).*$/, '\1'))
    doc_root = Pathname.new(Rails.root + 'app/views')
    template_path = File.dirname(caller_path.relative_path_from(doc_root).to_s) + '/' + arg
    begin
      render :template => template_path, :locals => locals
    rescue ActionView::MissingTemplate => exc
      "<!-- template missing %s -->" % template_path
    end
  end

  def page_title
    if @page
      @page.title
    else
      nil
    end
  end

  #
  # renders the content of a static page
  # this is an ugly duplicate of the application controller render_page, because
  # they call two different 'render' methods (controller and view renders behave differently).
  # TODO: figure out how to combine into one helper_method.
  #
  # def render_page(page)
  #   begin
  #     render :template => page.template_path
  #   rescue ActionView::MissingTemplate => exc
  #     begin
  #       render :template => page.template_path(DEFAULT_LOCALE)
  #     rescue
  #       raise exc
  #     end
  #   end
  # end

end
