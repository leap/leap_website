module BlogHelper

  def recent_blog_summaries(path)
    root = site.find_page(path)
    if root
      pages = root.all_children.order_by(:posted_at, :direction => :desc).limit(site.pagination_size)
      haml do
        pages.each do |page|
          haml render(:partial => 'layouts/blog/summary', :locals => {:page => page})
        end
      end
    end
  end

  def news_feed_link
    link_to(image_tag('/img/feed-icon-14x14.png'), "/#{I18n.locale}/news.atom")
  end
end
