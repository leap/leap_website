module NavigationHelper

  def has_side_column?
    if root_page? || site.menu.nil?
      return false
    end
    second_level_children_count = site.menu.submenu(current_page_path.first).try(:size)
    if second_level_children_count.nil?
      false
    else
      second_level_children_count >= 1
    end
  end

  def top_level_navigation_links
    return unless site.menu
    haml do
      first = 'first'
      site.menu.each do |item|
        active = current_page_path.first == item.name ? 'active' : ''
        haml 'li.tab', :class => first do
          haml 'a.tab', menu_item_title(item), :href => menu_item_path(item), :class => active
        end
        first = ''
      end
    end
  end

  def side_column_navigation_links
    if menu = site.menu.submenu(current_page_path.first)
      haml do
        haml 'ul.nav.nav-tabs.nav-stacked' do
          display_menu(menu, 1)
        end
      end
    end
  end

  def act_as(page)
    page = site.find_page(page)
    @current_page_path = page.path
    page_body(page)
  end

  def child_summaries(page=@page)
    return unless page
    menu = submenu_for_page(page)
    return unless menu
    haml do
      menu.children.each do |submenu|
        child_page = page.child(submenu.name)
        haml :h3 do
          haml :a, child_page.nav_title, :href => page_path(child_page)
        end
        haml :p, child_page.props.summary
      end
    end
  end

  private

  def menu_item_path(item)
    "/#{I18n.locale}/#{item.path.join('/')}"
  end

  def menu_item_title(item)
    page = site.pages[item.path_str] || site.pages[item.name]
    if page
      page.nav_title(I18n.locale)
    else
      nil
    end
  end

  def display_menu(menu, level=0)
    menu.each do |item|
      title = menu_item_title(item)
      if title
        haml 'li', :class => path_active_class(current_page_path, item) do
          haml 'a', menu_item_title(item), :href => menu_item_path(item), :class => "level#{level}"
        end
        if path_open?(current_page_path, item)
          display_menu(item.submenu, level+1)
        end
      end
    end
  end

  def path_active_class(page_path, menu_item)
    active = ''
    if menu_item.path == page_path
      active = 'active'
    elsif menu_item.path_prefix_of?(page_path)
      if menu_item.leaf_for_path?(page_path)
        active = 'active'
      else
        active = 'semi-active'
      end
    end
    active
  end

  def path_open?(page_path, menu_item)
    menu_item.path == page_path || menu_item.path_prefix_of?(page_path)
  end

  def current_page_path
    @current_page_path ||= begin
      if @page
        @page.path
      elsif params[:page].is_a? String
        params[:page].split('/')
      else
        []
      end
    end
  end

  #
  # the usage of 'home' as the default root page is hardcoded right now in the routes.
  # this should be changed in the future.
  #
  def root_page?
    @page && @page.path == ['home']
  end

  def submenu_for_page(page)
    menu = site.menu
    page.path.each do |segment|
      menu = menu.submenu(segment)
    end
    return menu
  rescue
    return nil
  end

end
