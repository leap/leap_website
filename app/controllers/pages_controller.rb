class PagesController < ApplicationController

  class PageNotFound < Exception; end

  layout :choose_layout
  rescue_from ActionView::MissingTemplate, :with => :render_404
  rescue_from PageNotFound, :with => :render_404
  #rescue_from Encoding::CompatibilityError do |exception|
  #  render_500(exception.to_s)
  #end

  def show
    @page = site.find_pages(params[:page])
    if @page
      respond_to do |format|
        format.html { render_page(@page) }
        format.atom { render_atom_feed(@page) }
      end
    else
      logger.error("ERROR: could not find page %s" % params[:page])
      raise PageNotFound.new
    end
  end

  protected

  def choose_layout
    if @page && @page.props && @page.props.layout
      @page.props.layout
    else
      'application'
    end
  end

  def render_atom_feed(root)
    if root
      @pages = root.all_children.order_by(:posted_at, :direction => :desc).limit(site.pagination_size)
      if @pages.any?
        render :file => 'layouts/blog/feed', :layout => false, :content_type => 'application/atom+xml'
      else
        render_404
      end
    end
  end

end

