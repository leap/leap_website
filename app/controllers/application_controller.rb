
class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :set_locale, :initialize_pages

  class << self
    attr_accessor :current_site # a class instance variable
  end

  if Rails.env.production?
    rescue_from Exception, :with => :render_500
    rescue_from ActionController::RoutingError, :with => :render_404
  end

  protected

  ##
  ## LOCALIZATION
  ##

  #
  # ensure that the locale is encoded as the url prefix and I18n.locale is set.
  #
  def set_locale
    if request.format == 'text/html'
      if params[:locale].nil?  || !AVAILABLE_LANGUAGES.include?(params[:locale])
        locale = HttpAcceptLanguage::compatible_language_from(request.headers['HTTP_ACCEPT_LANGUAGE'], AVAILABLE_LANGUAGES) || DEFAULT_LOCALE
        locale = locale.to_s.sub('-', '_').sub(/_\w\w/, '')
        if request.path == '/'
          url = '/' + locale
        else
          url = url_for(params.merge(:locale => locale))
        end
        redirect_to url
      else
        I18n.locale = params[:locale]
        self.default_url_options[:locale] = params[:locale]
      end
    end
  end

  #def default_url_options
  #  {:locale => locale }
  #end

  ##
  ## RENDERING
  ##

  def render_500(msg=nil)
    @message = msg
    render :template => 'errors/error', :status => 500
  end

  def render_404
    render :template => 'errors/not_found', :status => 404
  end

  #
  # renders the content of a static page
  #
  def render_page(page)
    render :text => page.render_to_string(self), :layout => true
  end
  helper_method :render_page

  #
  # same as render page, but returns the string without the layout
  #
  def page_body(page)
    page.render_to_string(self)
  end
  helper_method :page_body

  ##
  ## SITE
  ##

  #
  # if we ever make this code support multiple sites, this should depend on the request's domain
  #
  def site
    self.class.current_site ||= Site.new
  end
  helper_method :site

  def initialize_pages
    if Rails.env.development?
      site.load_pages
    else
      site.reload_pages_if_needed
    end
  end

  #def run_once(name, options={})
  #  key_name = "run_once_#{name}"
  #  if !Thread.current[key_name] || options[:unless]
  #    yield
  #  end
  #  Thread.current[key_name] = true
  #end


end
