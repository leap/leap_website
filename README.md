About this web application
============================

This repository is DEPRECATED, move on to https://0xacab.org/leap/leap_se !

The LEAP public website is lightweight rails application that just mostly
serves up (semi) static templates.

No databases are used or harmed in the making of this website.

The goal with this web app is to be as flexible and lightweight as possible,
and to support stellar localization.

There are numerous ruby tools to build static websites, but I found them to
not be good for localization and to be too inflexible.

Installation

    sudo apt-get install ruby ruby-dev rubygems
    gem install bundler
    git clone git://leap.se/leap_website
    cd leap_website
    bundle

Optional

    sudo apt-get install python-docutils    # for ReStructuredText support

Running in development mode

    cd leap_website
    rails server thin

Console examples

    rails console
    rails console production
    rails dbconsole

Rake takes

    rake doc:app

Running in production (with thin)

    bundle exec rake assets:precompile
    RAILS_ENV=production rails server thin


Modifying pages
======================================

Edit the HAML files found in leap_website/pages


Deploying
======================================

Manually
----------------

    rsync -a leap_website server:~
    ssh server
    cd leap_website
    bundle install --deployment

With Capistrano
---------------------

    Edit config/deploy.rb
    cap setup
    cap deploy

Running with Apache
------------------------

Install Passenger (mod_rack)

   sudo apt-get install libapache2-mod-passenger

Configure Apache

    <VirtualHost *:80>
      ServerName leap.se
      DocumentRoot /home/leap/leap_website/public
      <Directory /home/leap/leap_website/public>
        Allow from all
        Options -MultiViews
      </Directory>
    </VirtualHost>


Development
==============================

This app uses precompiled assets.

If you change a sass source file, you must run this before deploying:

  rake assets:precompile RAILS_ENV=production

To make the stylesheets autogenerate again in development mode, run this:

  rake assets:clean


Development Notes
=============================

Search
---------------------------

Maybe we should add search.

This appears to be the most appropriate search tool to use for this static webapp: http://xapian.org/docs/bindings/ruby/

Some notes on how flask does a similar thing::
* http://librelist.com/browser//flask/2012/3/21/maillinglist-archive-searchable/#ff1d34246a16845c410d2c9a411f5829
* https://github.com/mitsuhiko/flask/blob/website/flask_website/search.py
* https://github.com/mitsuhiko/flask/blob/website/flask_website/views/mailinglist.py

pure ruby, for ar: https://github.com/dougal/acts_as_indexed/

Other static CMS in ruby
-----------------------------

* http://nestacms.com/
* https://github.com/gma/nesta
* https://github.com/gma/nesta-rails
* https://github.com/quickleft/regulate -- git, engine, cms, rails


