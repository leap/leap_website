#
# site configuration
#

pages 'pages', :path => '/'

pages ['../leap_doc', '/home/website/leap_doc'], :path => '/docs'

@title = "leap.se"

@pagination_size = 20
