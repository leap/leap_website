@title = "Job posting for grant writer"

Overview
========================

LEAP Encryption Access Project (https://leap.se) is looking for a grant writer with a high-level understanding of the grant writing process, extensive experience writing successful proposals for highly competitive grants, excellent time management, and project management skills along with excellent communication skills. The Grant Writer will be expected to build and maintain relationships with foundations and other organizations offering grant opportunities, and must be highly self motivated. Experience and interest in online communication and internet freedom a big plus.

* Start Date: ASAP
* Rate: Competitive non-profit
* Time: Min 4 months, 50% time preferred.
* Please contact mcnair@leap.se

Responsibilities
======================

* Researching, developing and writing inquiries, letters an proposals requesting state and federal funds.

* Tracking and monitoring assigned proposals, including their deadlines, requirements and written reports.

* Identify potential funding sources including, but not limited to governmental programs, private foundations, corporation.

* Contact potential funding sources to discuss eligibility requirements, criteria, interest, and potential programs, and maintain ongoing relationships with funding contacts.

* Write grants and thoroughly compile required attachments for submission.

* Help develop grant calendar for each year based on research and past funding history.

* Create compelling, persuasive, well-structured grant narratives through the use of effective storytelling and superior prose--tying LEAP's needs to funder priorities.

* Research, develop, write, package and submit grant proposals.

* Effectively perform all aspects of grant development and submission, including tracking of information, soliciting and collecting support letters and memoranda of understanding, board resolutions, developing funder-compliant budgets, and coordinating with internal sources to elicit necessary data.

* Undertake research for the purposes of gathering statistical, analytical and anecdotal data, analyzing those data, and using such data strategically to create compelling grant proposal narratives--which use current, evidence-based, peer-reviewed models, when appropriate.

* Undertake research related to identification of funding sources to meet specific programmatic needs through use of internal resources (e.g., newsletters, grant indexes, Internet resources) and external resources (e.g., training opportunities, bidders' conferences). Determine "goodness of fit" based on funder & internal priorities.

* Collaborate with finance, programs and compliance to ensure grant reporting and tracking grant compliance.

Qualifications
======================

The ideal candidate will:

* Ability to interface with personnel in a professional manner.
* 3+ years of successful grant writing experience with federal and government grants, as well as private foundation experience.
* Ability to demonstrate accomplishments and success in grants being awarded.
* Reporting experience with OMB Circular A-133 preferred.
* Strong organizational and interpersonal skills.
* Ability to write accurate, compelling narrative that uses grammar and spelling correctly.
* Ability to perform under deadlines and tight schedules.
* Ability to digest and comply with detailed instructions a must as is the ability to prioritize in the context of multiple projects.
* Experience developing program budgets for grant submissions.
* Experience with free and open source software a big plus.

About LEAP
======================

LEAP Encryption Access Project is a non-profit dedicated to giving all internet users access to secure communication. Our focus is on adapting encryption technology to make it easy to use and widely available.

* We are a team of 10 dedicated and experienced anti-surveillance hackers.
* We started development in earnest on June 1 2011.
* We are globally distributed in Europe, South America, North America, and South Korea (Yep, it is hard to schedule meetings).
* Everything we do is licensed as free and open source software (GPL whenever possible).
* People of color, women, and queer people are strongly encouraged to apply.
