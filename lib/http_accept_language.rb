require 'http_accept_language/parser'

module HttpAcceptLanguage

   def self.compatible_language_from(header, languages)
     Parser.new(header).compatible_language_from(languages)
   end

end
