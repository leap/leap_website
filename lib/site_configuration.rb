#
# A class for a site's configuration.
# Site configuration file is eval'ed in the context of an instance of SiteConfiguration
#

require 'pathname'

class SiteConfiguration

  attr_accessor :title
  attr_accessor :pagination_size
  attr_accessor :mount_points
  attr_reader :file_path

  ##
  ## CLASS METHODS
  ##

  def self.load(config_file)
    SiteConfiguration.new(config_file)
  end

  ##
  ## INSTANCE METHODS
  ##

  #
  # accepts a file_path to a configuration file.
  #
  def initialize(file_path)
    @file_path = file_path
    @site_title = "untitled"
    @pagination_size = 20
    @mount_points = []
    self.eval
  end

  def pages(directory_source, options={})
    @mount_points << SiteMountPoint.new(self, directory_source, options)
  end

  def pages_changed?
    @mount_points.detect {|mp| mp.changed?}
  end

  def eval
    self.instance_eval(File.read(@file_path), @file_path)
  end

end
