#
# A site can have many 'mount points' -- places in the site tree where different directories are inserted.
#
# At a minimum, every site needs a mount point for '/'
#
class SiteMountPoint

  attr_accessor :directory
  attr_accessor :relative_directory
  attr_accessor :path
  attr_accessor :options
  attr_accessor :menu_file
  attr_accessor :locales_dir
  attr_accessor :timestamp

  def initialize(site_config, directory_source, options={})
    @directory          = find_directory(site_config, directory_source)
    @path               = options[:path]
    @relative_directory = relative_dir_path(@directory)
    @menu_file          = file_path('menu.txt')
    @locales_dir        = file_path('locales')
    @options            = options
    reset_timestamp
  end

  def changed?
    File.mtime(@directory) > @timestamp
  end

  def reset_timestamp
    @timestamp = File.mtime(@directory)
  end

  private

  def file_path(file)
    path = File.join(@directory, file)
    if File.exists?(path)
      path
    else
      nil
    end
  end

  #
  # returns path relative to app/views
  #
  def relative_dir_path(directory)
    if Rails.root
      Pathname.new(directory).relative_path_from(Pathname.new(Rails.root + 'app/views')).to_s
    end
  end

  def find_directory(site_config, directory_source)
    directory = nil
    [directory_source].flatten.each do |dir_source|
      if dir_source.starts_with?('/')
        directory = dir_source
      else
        directory = File.expand_path(dir_source, File.dirname(site_config.file_path))
      end
      if File.directory?(directory)
        break
      end
    end
    if File.directory?(directory)
      return directory
    else
      raise 'FATAL ERROR: could find any of %s' % directory_source.join(', ')
    end
  end

end