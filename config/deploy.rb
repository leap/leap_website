require "bundler/capistrano"

set :application, "leap-website"
set :deploy_to, "/home/website/leap-website"
set :user, "website"

set :scm, :git
set :repository,  "ssh://gitolite@leap.se:22/leap_website"
set :branch, "master"
set :deploy_via, :remote_cache

ssh_options[:forward_agent] = true
ssh_options[:port] = 22

role :web, "leap.se"
role :app, "leap.se"

set :use_sudo, false

set :keep_releases, 10
after "deploy:restart", "deploy:cleanup"

namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "touch #{File.join(current_path,'tmp','restart.txt')}"
  end
end

namespace :leap do
  task :link_to_chiliproject do
    run "rm -f #{current_release}/public/code"
    run "ln -s /var/www/chili/public #{current_release}/public/code"
  end
  task :link_to_slides do
    run "rm -f #{current_release}/public/slides"
    run "ln -s /home/website/slides #{current_release}/public/slides"
  end
end

after  "deploy:create_symlink", "leap:link_to_chiliproject"
after  "deploy:create_symlink", "leap:link_to_slides"
