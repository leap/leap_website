# encoding: utf-8
# https://en.wikipedia.org/wiki/List_of_languages_by_number_of_native_speakers

LANGUAGES = {
  :zh => ['中文',       'zh', 1, false],
  :es => ['Español',   'es', 2, false],
  :en => ['English',   'en', 3, false],
  :ar => ['العربية',   'ar', 5, true],
  :pt => ['Português', 'pt', 6, false],
  :ru => ['Pyccĸий',   'ru', 7, false],
  :de => ['Deutsch',   'de', 8, false],
  :fr => ['Français',  'fr', 10, false],
  :it => ['Italiano',  'it', 11, false],
  :el => ['Ελληνικά',  'el', 20, false]
}

# although everywhere else we use symbols for locales, this array should be strings:
AVAILABLE_LANGUAGES = ['zh', 'es', 'en', 'ar', 'pt', 'ru', 'de', 'fr', 'it', 'el']

DEFAULT_LOCALE = :en