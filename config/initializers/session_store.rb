# Be sure to restart your server when you modify this file.

LeapPublicSite::Application.config.session_store :disabled #:cookie_store, :key => '_leap-public-site_session'

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rails generate session_migration")
# LeapPublicSite::Application.config.session_store :active_record_store
